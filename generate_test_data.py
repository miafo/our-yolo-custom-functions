import os

image_files = []
coord_files = []
num_index = len("./data/our_dataset/test_frame")
os.chdir(os.path.join("data", "our_dataset"))
for filename in os.listdir(os.getcwd()):
    if filename.endswith(".jpg"):
        image_files.append("./data/our_dataset/" + filename)
    else:
        coord_files.append("./data/our_dataset/" + filename)
os.chdir("..")
with open("test-data.txt", "w") as outfile:
    for image in sorted(image_files, key=lambda x: int(x[num_index:-4])):
        outfile.write(image)
        outfile.write("\n")
    outfile.close()
with open("test-data-coord.txt", "w") as outfile:
    for file in sorted(coord_files, key=lambda x: int(x[num_index:-4])):
        outfile.write(file)
        outfile.write("\n")
    outfile.close()
os.chdir("..")
